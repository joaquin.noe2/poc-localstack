const { SQSClient, SendMessageCommand } = require('@aws-sdk/client-sqs');

const pushToSQS = async (queueURL, messageBody) => {
    const client = new SQSClient({ endpoint: "http://localhost:4566" });
    const params = {
        QueueUrl: queueURL,
        MessageBody: messageBody,
    };
    const data = await client.send(new SendMessageCommand(params));
    console.log(`Message sent with ID: ${data.MessageId}`);

};

module.exports = {
    pushToSQS,
}