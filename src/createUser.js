// Import users.service.js
const UserService = require("./users/users.service");

module.exports.handler = async (event) => {
  const userService = new UserService();
  try {
    await userService.create({
      id: "1",
      name: "John Doe",
      email: "joendoe@email.com",
      password: "thisisasecret",
    });
    return {
      statusCode: 200,
      body: JSON.stringify(
        {
          message: "User created",
        },
        null,
        2
      ),
    };
  } catch (err) {
    return {
      statusCode: 500,
      body: JSON.stringify(
        {
          message: "Error creating user",
          err: err,
        },
        null,
        2
      ),
    };
  }
};
