const { DynamoDBClient, PutItemCommand } = require('@aws-sdk/client-dynamodb');
const {pushToSQS} = require('../utils/sqs');

class UserService {
    constructor() {
        this.dynamodb = new DynamoDBClient({ region: 'us-east-1', endpoint: 'http://localhost:4566' });
    }

    // Create a new user.
    async create(user = {}) {
        const params = this.buildParams(user);
        const result = await this.dynamodb.send(new PutItemCommand(params));
        await pushToSQS('http://localhost:4566/000000000000/user-created', JSON.stringify(user));
        return result;
    }

    buildParams(user = {}) {
        return {
            TableName: 'User',
            Item: {
                'id': { S: user.id },
                'name': { S: user.name },
                'email': { S: user.email },
                'password': { S: user.password },
            }
        };
    }
}

module.exports = UserService;